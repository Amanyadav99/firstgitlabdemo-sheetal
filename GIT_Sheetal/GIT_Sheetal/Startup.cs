﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GIT_Sheetal.Startup))]
namespace GIT_Sheetal
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
